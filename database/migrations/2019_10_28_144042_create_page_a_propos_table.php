<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePageAProposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_a_propos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('slide')->nullable();
            $table->string('slide_title')->nullable();
            $table->string('image')->nullable();
            $table->string('titre')->nullable();
            $table->text('description')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page_a_propos');
    }
}
