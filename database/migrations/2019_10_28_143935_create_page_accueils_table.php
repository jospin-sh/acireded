<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePageAccueilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_accueils', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('slide')->nullable();
            $table->string('slide_title')->nullable();
            $table->string('lien_video')->nullable();
            $table->integer('user_id')->unsigned();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page_accueils');
    }
}
