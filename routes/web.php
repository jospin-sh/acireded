<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Clear Config cache:
Route::get('/clear', 'CacheController@clear')->name('clear');
Route::get('/cache', 'CacheController@cache')->name('cache');

Route::get('/', 'SiteController@index')->name('homepage');
Route::get('/home', 'SiteController@index')->name('home');
Route::get('/about', 'SiteController@about')->name('about');
Route::get('/blog', 'SiteController@blog')->name('blog');
Route::get('/causes', 'SiteController@causes')->name('causes');
Route::get('/contact', 'SiteController@contact')->name('contact');
Route::post('/contact', 'SiteController@contactUs')->name('contactus');
Route::get('/don', 'SiteController@donate')->name('don');
Route::get('/event', 'SiteController@event')->name('event');
Route::get('/gallery', 'SiteController@gallery')->name('gallery');

Route::group(['prefix'=>'admin'], function(){
    Route::group(['middleware'=>'auth'], function(){
       // Route::resource('sites', 'Admin\\SitesController');
        Route::resource('cause', 'Admin\\CauseController');
        Route::resource('don', 'Admin\\DonController');
        Route::resource('evenement', 'Admin\\EvenementController');
        Route::resource('image-gallerie', 'Admin\\ImageGallerieController');
        Route::resource('page-accueil', 'Admin\\PageAccueilController');
        Route::resource('page-a-propos', 'Admin\\PageAProposController');
        Route::resource('page-blog', 'Admin\\PageBlogController');
        Route::resource('page-blog-detail', 'Admin\\PageBlogDetailController');
        Route::resource('page-causes', 'Admin\\PageCausesController');
        Route::resource('page-causes-detail', 'Admin\\PageCausesDetailController');
        Route::resource('page-donation', 'Admin\\PageDonationController');
        Route::resource('page-event', 'Admin\\PageEventController');
        Route::resource('page-gallerie', 'Admin\\PageGallerieController');
        Route::resource('post', 'Admin\\PostController');
        Route::resource('site', 'Admin\\SitesController');
    });
    Route::get('/', 'Auth\\LoginController@showLoginForm')->name('admin_home');
     Auth::routes();
});