<?php

namespace App\Providers;

use App\Post;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        setlocale(LC_TIME, config('fr'));
        Schema::defaultStringLength(191);

        view()->composer('layouts.app-site', function ($view) {
            $posts = Post::take(3)->get();
            $view->with('posts', $posts);
        });
    }
}
