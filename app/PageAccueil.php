<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageAccueil extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'page_accueils';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['slide', 'slide_title', 'lien_video', 'user_id'];

    
}
