<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Don extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dons';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['image', 'nom', 'montant', 'date', 'cause'];

    
}
