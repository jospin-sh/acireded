<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cause extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'causes';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['titre', 'description', 'image', 'montant_percu', 'montant_voulu'];

    
}
