<?php

namespace App\Http\Controllers;

use App\Cause;
use App\Constantes;
use App\Contact;
use App\Evenement;
use App\Http\Controllers\Admin\PageAccueilController;
use App\ImageGallerie;
use App\Mail\ContactMail;
use App\PageAccueil;
use App\PageAPropo;
use App\PageBlog;
use App\PageDonation;
use App\PageEvent;
use App\PageGallerie;
use App\PageCause;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Butschster\Head\Facades\Meta;

class SiteController extends Controller
{
    public function index(){

        $page = PageAccueil::first();
        $galleries = ImageGallerie::take(8)->get();
        $causes = Cause::all();
        $posts = Post::take(3)->get();
        $events = Evenement::take(3)->get();

        /*Meta::appendTitle('Accueil')
           // ->setPaginationLinks($galleries)
            //->setFavicon('/favicon-index.ico')
            ->addMeta('robots', ['content' => 'noindex']);*/

        return view('site/home', compact('page', 'galleries', 'causes', 'posts', 'events'));
    }

    public function about(){
        $page = PageAPropo::first();
        return view('site/about', ['page' => $page]);
    }

    public function blog(){
        $page = PageBlog::first();
        $posts = Post::all();
        return view('site/blog', compact('page', 'posts'));
    }

    public function causes(){
        $page = PageCause::first();
        $causes = Cause::all();
        return view('site/causes', ['page' => $page, 'causes' => $causes]);
    }

    public function contact(){
        $page = PageAPropo::first();
        return view('site/contact');
    }

    public function donate(){
        $page = PageDonation::first();
        return view('site/donate', ['page' => $page]);
    }

    public function event(){
        $page = PageEvent::first();
        $events = Evenement::all();
        return view('site/event', compact('page', 'events'));
    }

    public function gallery(){
        $page = PageGallerie::first();
        $galleries = ImageGallerie::get();
        return view('site/gallery', compact('page', 'galleries'));
    }

    public function contactUs(Request $request){
        $this->validate($request, [
            'name' => 'required|max:50',
            'email' => 'required|max:50',
            'subject' => 'required',
            'message' => 'required'
        ]);
        $contact = new Contact();
        $contact->setName($request->name);
        $contact->setEmail($request->email);
        $contact->setSubject($request->subject);
        $contact->setMessage($request->message);

        $contact->mailSubject = "Nouveau message depuis votre site";
        $mailTo = env("MAIL_USERNAME", "contact@acireded.com");
        Mail::to($mailTo)->send(new ContactMail($contact));
        return redirect('contact')->with('success', 'Votre message a été envoyé avec succès.');
        //return response()->json(["OK"]);
    }
}
