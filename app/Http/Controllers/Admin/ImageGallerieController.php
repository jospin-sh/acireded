<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\ImageGallerie;
use Illuminate\Http\Request;

class ImageGallerieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $imagegallerie = ImageGallerie::where('image', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $imagegallerie = ImageGallerie::latest()->paginate($perPage);
        }

        return view('admin.image-gallerie.index', compact('imagegallerie'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.image-gallerie.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'image' => 'required'
		]);
        $requestData = $request->all();
                if ($request->hasFile('image')) {
            $requestData['image'] = $request->file('image')
                ->store('uploads', 'public');
        }

        ImageGallerie::create($requestData);

        return redirect('admin/image-gallerie')->with('flash_message', 'ImageGallerie added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $imagegallerie = ImageGallerie::findOrFail($id);

        return view('admin.image-gallerie.show', compact('imagegallerie'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $imagegallerie = ImageGallerie::findOrFail($id);

        return view('admin.image-gallerie.edit', compact('imagegallerie'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'image' => 'required'
		]);
        $requestData = $request->all();
                if ($request->hasFile('image')) {
            $requestData['image'] = $request->file('image')
                ->store('uploads', 'public');
        }

        $imagegallerie = ImageGallerie::findOrFail($id);
        $imagegallerie->update($requestData);

        return redirect('admin/image-gallerie')->with('flash_message', 'ImageGallerie updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        ImageGallerie::destroy($id);

        return redirect('admin/image-gallerie')->with('flash_message', 'ImageGallerie deleted!');
    }
}
