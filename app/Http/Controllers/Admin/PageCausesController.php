<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\PageCause;
use Illuminate\Http\Request;

class PageCausesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $pagecauses = PageCause::where('slide', 'LIKE', "%$keyword%")
                ->orWhere('slide_title', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $pagecauses = PageCause::latest()->paginate($perPage);
        }

        return view('admin.page-causes.index', compact('pagecauses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.page-causes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
                if ($request->hasFile('slide')) {
            $requestData['slide'] = $request->file('slide')
                ->store('uploads', 'public');
        }

        PageCause::create($requestData);

        return redirect('admin/page-causes')->with('flash_message', 'PageCause added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $pagecause = PageCause::findOrFail($id);

        return view('admin.page-causes.show', compact('pagecause'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $pagecause = PageCause::findOrFail($id);

        return view('admin.page-causes.edit', compact('pagecause'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
                if ($request->hasFile('slide')) {
            $requestData['slide'] = $request->file('slide')
                ->store('uploads', 'public');
        }

        $pagecause = PageCause::findOrFail($id);
        $pagecause->update($requestData);

        return redirect('admin/page-causes')->with('flash_message', 'PageCause updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        PageCause::destroy($id);

        return redirect('admin/page-causes')->with('flash_message', 'PageCause deleted!');
    }
}
