<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\PageGallerie;
use Illuminate\Http\Request;

class PageGallerieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $pagegallerie = PageGallerie::where('slide', 'LIKE', "%$keyword%")
                ->orWhere('slide_title', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $pagegallerie = PageGallerie::latest()->paginate($perPage);
        }

        return view('admin.page-gallerie.index', compact('pagegallerie'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.page-gallerie.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
                if ($request->hasFile('slide')) {
            $requestData['slide'] = $request->file('slide')
                ->store('uploads', 'public');
        }

        PageGallerie::create($requestData);

        return redirect('admin/page-gallerie')->with('flash_message', 'PageGallerie added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $pagegallerie = PageGallerie::findOrFail($id);

        return view('admin.page-gallerie.show', compact('pagegallerie'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $pagegallerie = PageGallerie::findOrFail($id);

        return view('admin.page-gallerie.edit', compact('pagegallerie'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
                if ($request->hasFile('slide')) {
            $requestData['slide'] = $request->file('slide')
                ->store('uploads', 'public');
        }

        $pagegallerie = PageGallerie::findOrFail($id);
        $pagegallerie->update($requestData);

        return redirect('admin/page-gallerie')->with('flash_message', 'PageGallerie updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        PageGallerie::destroy($id);

        return redirect('admin/page-gallerie')->with('flash_message', 'PageGallerie deleted!');
    }
}
