<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Evenement;
use Illuminate\Http\Request;

class EvenementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $evenement = Evenement::where('image', 'LIKE', "%$keyword%")
                ->orWhere('titre', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('date', 'LIKE', "%$keyword%")
                ->orWhere('heure_debut', 'LIKE', "%$keyword%")
                ->orWhere('heure_fin', 'LIKE', "%$keyword%")
                ->orWhere('lieu', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $evenement = Evenement::latest()->paginate($perPage);
        }

        return view('admin.evenement.index', compact('evenement'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.evenement.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'titre' => 'required',
			'description' => 'required',
			'date' => 'required',
			'heure_debut' => 'required',
			'heure_fin' => 'required',
			'lieu' => 'required'
		]);
        $requestData = $request->all();
                if ($request->hasFile('image')) {
            $requestData['image'] = $request->file('image')
                ->store('uploads', 'public');
        }

        Evenement::create($requestData);

        return redirect('admin/evenement')->with('flash_message', 'Evenement added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $evenement = Evenement::findOrFail($id);

        return view('admin.evenement.show', compact('evenement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $evenement = Evenement::findOrFail($id);

        return view('admin.evenement.edit', compact('evenement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'titre' => 'required',
			'description' => 'required',
			'date' => 'required',
			'heure_debut' => 'required',
			'heure_fin' => 'required',
			'lieu' => 'required'
		]);
        $requestData = $request->all();
                if ($request->hasFile('image')) {
            $requestData['image'] = $request->file('image')
                ->store('uploads', 'public');
        }

        $evenement = Evenement::findOrFail($id);
        $evenement->update($requestData);

        return redirect('admin/evenement')->with('flash_message', 'Evenement updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Evenement::destroy($id);

        return redirect('admin/evenement')->with('flash_message', 'Evenement deleted!');
    }
}
