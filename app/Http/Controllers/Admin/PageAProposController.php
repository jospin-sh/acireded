<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\PageAPropo;
use Illuminate\Http\Request;

class PageAProposController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $pageapropos = PageAPropo::where('slide', 'LIKE', "%$keyword%")
                ->orWhere('slide_title', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->orWhere('titre', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $pageapropos = PageAPropo::latest()->paginate($perPage);
        }

        return view('admin.page-a-propos.index', compact('pageapropos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.page-a-propos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
                if ($request->hasFile('slide')) {
            $requestData['slide'] = $request->file('slide')
                ->store('uploads', 'public');
        }
        if ($request->hasFile('image')) {
            $requestData['image'] = $request->file('image')
                ->store('uploads', 'public');
        }

        PageAPropo::create($requestData);

        return redirect('admin/page-a-propos')->with('flash_message', 'PageAPropo added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $pageapropo = PageAPropo::findOrFail($id);

        return view('admin.page-a-propos.show', compact('pageapropo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $pageapropo = PageAPropo::findOrFail($id);

        return view('admin.page-a-propos.edit', compact('pageapropo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
                if ($request->hasFile('slide')) {
            $requestData['slide'] = $request->file('slide')
                ->store('uploads', 'public');
        }
        if ($request->hasFile('image')) {
            $requestData['image'] = $request->file('image')
                ->store('uploads', 'public');
        }

        $pageapropo = PageAPropo::findOrFail($id);
        $pageapropo->update($requestData);

        return redirect('admin/page-a-propos')->with('flash_message', 'PageAPropo updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        PageAPropo::destroy($id);

        return redirect('admin/page-a-propos')->with('flash_message', 'PageAPropo deleted!');
    }
}
