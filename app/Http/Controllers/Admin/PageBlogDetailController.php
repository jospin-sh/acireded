<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\PageBlogDetail;
use Illuminate\Http\Request;

class PageBlogDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $pageblogdetail = PageBlogDetail::where('slide', 'LIKE', "%$keyword%")
                ->orWhere('slide_title', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $pageblogdetail = PageBlogDetail::latest()->paginate($perPage);
        }

        return view('admin.page-blog-detail.index', compact('pageblogdetail'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.page-blog-detail.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
                if ($request->hasFile('slide')) {
            $requestData['slide'] = $request->file('slide')
                ->store('uploads', 'public');
        }

        PageBlogDetail::create($requestData);

        return redirect('admin/page-blog-detail')->with('flash_message', 'PageBlogDetail added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $pageblogdetail = PageBlogDetail::findOrFail($id);

        return view('admin.page-blog-detail.show', compact('pageblogdetail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $pageblogdetail = PageBlogDetail::findOrFail($id);

        return view('admin.page-blog-detail.edit', compact('pageblogdetail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
                if ($request->hasFile('slide')) {
            $requestData['slide'] = $request->file('slide')
                ->store('uploads', 'public');
        }

        $pageblogdetail = PageBlogDetail::findOrFail($id);
        $pageblogdetail->update($requestData);

        return redirect('admin/page-blog-detail')->with('flash_message', 'PageBlogDetail updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        PageBlogDetail::destroy($id);

        return redirect('admin/page-blog-detail')->with('flash_message', 'PageBlogDetail deleted!');
    }
}
