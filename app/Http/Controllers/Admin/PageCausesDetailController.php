<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\PageCausesDetail;
use Illuminate\Http\Request;

class PageCausesDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $pagecausesdetail = PageCausesDetail::where('slide', 'LIKE', "%$keyword%")
                ->orWhere('slide_title', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $pagecausesdetail = PageCausesDetail::latest()->paginate($perPage);
        }

        return view('admin.page-causes-detail.index', compact('pagecausesdetail'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.page-causes-detail.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
                if ($request->hasFile('slide')) {
            $requestData['slide'] = $request->file('slide')
                ->store('uploads', 'public');
        }

        PageCausesDetail::create($requestData);

        return redirect('admin/page-causes-detail')->with('flash_message', 'PageCausesDetail added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $pagecausesdetail = PageCausesDetail::findOrFail($id);

        return view('admin.page-causes-detail.show', compact('pagecausesdetail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $pagecausesdetail = PageCausesDetail::findOrFail($id);

        return view('admin.page-causes-detail.edit', compact('pagecausesdetail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
                if ($request->hasFile('slide')) {
            $requestData['slide'] = $request->file('slide')
                ->store('uploads', 'public');
        }

        $pagecausesdetail = PageCausesDetail::findOrFail($id);
        $pagecausesdetail->update($requestData);

        return redirect('admin/page-causes-detail')->with('flash_message', 'PageCausesDetail updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        PageCausesDetail::destroy($id);

        return redirect('admin/page-causes-detail')->with('flash_message', 'PageCausesDetail deleted!');
    }
}
