<div class="form-group {{ $errors->has('slide') ? 'has-error' : ''}}">
    <label for="slide" class="control-label">{{ 'Slide' }}</label>
    <input class="form-control" name="slide" type="file" id="slide" value="{{ isset($pageapropo->slide) ? $pageapropo->slide : ''}}" >
    {!! $errors->first('slide', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('slide_title') ? 'has-error' : ''}}">
    <label for="slide_title" class="control-label">{{ 'Slide Title' }}</label>
    <input class="form-control" name="slide_title" type="text" id="slide_title" value="{{ isset($pageapropo->slide_title) ? $pageapropo->slide_title : ''}}" >
    {!! $errors->first('slide_title', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    <label for="image" class="control-label">{{ 'Image' }}</label>
    <input class="form-control" name="image" type="file" id="image" value="{{ isset($pageapropo->image) ? $pageapropo->image : ''}}" >
    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('titre') ? 'has-error' : ''}}">
    <label for="titre" class="control-label">{{ 'Titre' }}</label>
    <input class="form-control" name="titre" type="text" id="titre" value="{{ isset($pageapropo->titre) ? $pageapropo->titre : ''}}" >
    {!! $errors->first('titre', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    <label for="description" class="control-label">{{ 'Description' }}</label>
    <textarea class="form-control" name="description" type="text" id="description">{{ isset($pageapropo->description) ? $pageapropo->description : ''}}</textarea>
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
