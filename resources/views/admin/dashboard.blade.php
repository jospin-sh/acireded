@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')
            {!! csrf_field() !!}

        </div>
    </div>
@endsection