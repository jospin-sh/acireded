<div class="form-group {{ $errors->has('slide') ? 'has-error' : ''}}">
    <label for="slide" class="control-label">{{ 'Slide' }}</label>
    <input class="form-control" name="slide" type="file" id="slide" value="{{ isset($pageaccueil->slide) ? $pageaccueil->slide : ''}}" >
    {!! $errors->first('slide', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('slide_title') ? 'has-error' : ''}}">
    <label for="slide_title" class="control-label">{{ 'Slide Title' }}</label>
    <input class="form-control" name="slide_title" type="text" id="slide_title" value="{{ isset($pageaccueil->slide_title) ? $pageaccueil->slide_title : ''}}" >
    {!! $errors->first('slide_title', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('lien_video') ? 'has-error' : ''}}">
    <label for="lien_video" class="control-label">{{ 'Lien Video' }}</label>
    <input class="form-control" name="lien_video" type="text" id="lien_video" value="{{ isset($pageaccueil->lien_video) ? $pageaccueil->lien_video : ''}}" >
    {!! $errors->first('lien_video', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
    <label for="user_id" class="control-label">{{ 'User Id' }}</label>
    <input class="form-control" name="user_id" type="number" id="user_id" value="{{ isset($pageaccueil->user_id) ? $pageaccueil->user_id : ''}}" >
    {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
