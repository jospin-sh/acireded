@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">PageAccueil {{ $pageaccueil->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/page-accueil') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/page-accueil/' . $pageaccueil->id . '/edit') }}" title="Edit PageAccueil"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('admin/pageaccueil' . '/' . $pageaccueil->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete PageAccueil" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $pageaccueil->id }}</td>
                                    </tr>
                                    <tr><th> Slide </th><td> {{ $pageaccueil->slide }} </td></tr><tr><th> Slide Title </th><td> {{ $pageaccueil->slide_title }} </td></tr><tr><th> Lien Video </th><td> {{ $pageaccueil->lien_video }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
