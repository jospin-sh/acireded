<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    <label for="image" class="control-label">{{ 'Image' }}</label>
    <input class="form-control" name="image" type="file" id="image" value="{{ isset($don->image) ? $don->image : ''}}" >
    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nom') ? 'has-error' : ''}}">
    <label for="nom" class="control-label">{{ 'Nom' }}</label>
    <input class="form-control" name="nom" type="text" id="nom" value="{{ isset($don->nom) ? $don->nom : ''}}" required>
    {!! $errors->first('nom', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('montant') ? 'has-error' : ''}}">
    <label for="montant" class="control-label">{{ 'Montant' }}</label>
    <input class="form-control" name="montant" type="file" id="montant" value="{{ isset($don->montant) ? $don->montant : ''}}" required>
    {!! $errors->first('montant', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('date') ? 'has-error' : ''}}">
    <label for="date" class="control-label">{{ 'Date' }}</label>
    <input class="form-control" name="date" type="date" id="date" value="{{ isset($don->date) ? $don->date : ''}}" required>
    {!! $errors->first('date', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('cause') ? 'has-error' : ''}}">
    <label for="cause" class="control-label">{{ 'Cause' }}</label>
    <input class="form-control" name="cause" type="text" id="cause" value="{{ isset($don->cause) ? $don->cause : ''}}" required>
    {!! $errors->first('cause', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
