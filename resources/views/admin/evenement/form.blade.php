<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    <label for="image" class="control-label">{{ 'Image' }}</label>
    <input class="form-control" name="image" type="file" id="image" value="{{ isset($evenement->image) ? $evenement->image : ''}}" >
    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('titre') ? 'has-error' : ''}}">
    <label for="titre" class="control-label">{{ 'Titre' }}</label>
    <input class="form-control" name="titre" type="text" id="titre" value="{{ isset($evenement->titre) ? $evenement->titre : ''}}" required>
    {!! $errors->first('titre', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    <label for="description" class="control-label">{{ 'Description' }}</label>
    <textarea class="form-control" rows="5" name="description" type="textarea" id="description" required>{{ isset($evenement->description) ? $evenement->description : ''}}</textarea>
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('date') ? 'has-error' : ''}}">
    <label for="date" class="control-label">{{ 'Date' }}</label>
    <input class="form-control" name="date" type="date" id="date" value="{{ isset($evenement->date) ? $evenement->date : ''}}" required>
    {!! $errors->first('date', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('heure_debut') ? 'has-error' : ''}}">
    <label for="heure_debut" class="control-label">{{ 'Heure Debut' }}</label>
    <input class="form-control" name="heure_debut" type="time" id="heure_debut" value="{{ isset($evenement->heure_debut) ? $evenement->heure_debut : ''}}" required>
    {!! $errors->first('heure_debut', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('heure_fin') ? 'has-error' : ''}}">
    <label for="heure_fin" class="control-label">{{ 'Heure Fin' }}</label>
    <input class="form-control" name="heure_fin" type="time" id="heure_fin" value="{{ isset($evenement->heure_fin) ? $evenement->heure_fin : ''}}" required>
    {!! $errors->first('heure_fin', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('lieu') ? 'has-error' : ''}}">
    <label for="lieu" class="control-label">{{ 'Lieu' }}</label>
    <input class="form-control" name="lieu" type="text" id="lieu" value="{{ isset($evenement->lieu) ? $evenement->lieu : ''}}" required>
    {!! $errors->first('lieu', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
