<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    <label for="image" class="control-label">{{ 'Image' }}</label>
    <input class="form-control" name="image" type="file" id="image" value="{{ isset($post->image) ? $post->image : ''}}" >
    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('titre') ? 'has-error' : ''}}">
    <label for="titre" class="control-label">{{ 'Titre' }}</label>
    <input class="form-control" name="titre" type="text" id="titre" value="{{ isset($post->titre) ? $post->titre : ''}}" >
    {!! $errors->first('titre', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('categorie') ? 'has-error' : ''}}">
    <label for="categorie" class="control-label">{{ 'Categorie' }}</label>
    <input class="form-control" name="categorie" type="text" id="categorie" value="{{ isset($post->categorie) ? $post->categorie : ''}}" >
    {!! $errors->first('categorie', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('cause') ? 'has-error' : ''}}">
    <label for="cause" class="control-label">{{ 'Cause' }}</label>
    <input class="form-control" name="cause" type="text" id="cause" value="{{ isset($post->cause) ? $post->cause : ''}}" required>
    {!! $errors->first('cause', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('contenu') ? 'has-error' : ''}}">
    <label for="cause" class="control-label">{{ 'Contenu' }}</label>
    <textarea class="form-control" name="contenu" id="contenu" required>{{ isset($post->contenu) ? $post->contenu : ''}}</textarea>
    {!! $errors->first('contenu', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('date') ? 'has-error' : ''}}">
    <label for="date" class="control-label">{{ 'Date' }}</label>
    <input class="form-control" name="date" type="date" id="date" value="{{ isset($post->date) ? $post->date : ''}}" required>
    {!! $errors->first('date', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
    <label for="user_id" class="control-label">{{ 'User Id' }}</label>
    <input class="form-control" name="user_id" type="number" id="user_id" value="{{ isset($post->user_id) ? $post->user_id : ''}}" >
    {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
