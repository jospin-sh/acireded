<div class="form-group {{ $errors->has('titre') ? 'has-error' : ''}}">
    <label for="titre" class="control-label">{{ 'Titre' }}</label>
    <input class="form-control" name="titre" type="text" id="titre" value="{{ isset($cause->titre) ? $cause->titre : ''}}" required>
    {!! $errors->first('titre', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    <label for="description" class="control-label">{{ 'Description' }}</label>
    <textarea class="form-control" rows="5" name="description" type="textarea" id="description" required>{{ isset($cause->description) ? $cause->description : ''}}</textarea>
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    <label for="image" class="control-label">{{ 'Image' }}</label>
    <input class="form-control" name="image" type="file" id="image" value="{{ isset($cause->image) ? $cause->image : ''}}" >
    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('montant_percu') ? 'has-error' : ''}}">
    <label for="montant_percu" class="control-label">{{ 'Montant Percu' }}</label>
    <input class="form-control" name="montant_percu" type="number" id="montant_percu" value="{{ isset($cause->montant_percu) ? $cause->montant_percu : ''}}" >
    {!! $errors->first('montant_percu', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('montant_voulu') ? 'has-error' : ''}}">
    <label for="montant_voulu" class="control-label">{{ 'Montant Voulu' }}</label>
    <input class="form-control" name="montant_voulu" type="number" id="montant_voulu" value="{{ isset($cause->montant_voulu) ? $cause->montant_voulu : ''}}" >
    {!! $errors->first('montant_voulu', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
