<div class="col-md-3">
    <div class="card">
        <div class="card-header">
            Menu
        </div>

        <div class="card-body pd-r-0 pd-t-0">
            <li class="as-nav @if(request()->routeIs('site*')) as-active @endif"
                role="presentation">
                <a href="{{ route('site.index') }}">
                    Infos générales
                </a>
            </li>
            <ul class="nav" role="tablist">
                <li class="as-nav @if(request()->routeIs('page-accueil*')) as-active @endif"
                    role="presentation">
                    <a href="{{ route('page-accueil.edit', 1) }}">
                        Page Accueil
                    </a>
                </li>
                <li class="as-nav @if(request()->routeIs('page-a-propos*')) as-active @endif"
                    role="presentation">
                    <a href="{{ route('page-a-propos.edit', 1) }}">
                        Page à propos
                    </a>
                </li>
                <li class="as-nav @if(request()->routeIs('page-causes.edit')) as-active @endif"
                    role="presentation">
                    <a href="{{ route('page-causes.edit', 1) }}">
                        Page Causes
                    </a>
                </li>
                <li class="as-nav @if(request()->routeIs('page-causes-detail*')) as-active @endif"
                    role="presentation">
                    <a href="{{ route('page-causes-detail.edit', 1) }}">
                        page causes detail
                    </a>
                </li>
                <li class="as-nav @if(request()->routeIs('page-donation*')) as-active @endif"
                    role="presentation">
                    <a href="{{ route('page-donation.edit', 1) }}">
                        page donation
                    </a>
                </li>
                <li class="as-nav @if(request()->routeIs('page-blog.index') || request()->routeIs('site.index')) as-active @endif"
                    role="presentation">
                    <a href="{{ route('page-blog.edit', 1) }}">
                        Page Blog
                    </a>
                </li>
                <li class="as-nav @if(request()->routeIs('page-blog-detail*')) as-active @endif"
                    role="presentation">
                    <a href="{{ route('page-blog-detail.edit', 1) }}">
                        Page Blog détails
                    </a>
                </li>
                <li class="as-nav @if(request()->routeIs('page-gallerie*')) as-active @endif"
                    role="presentation" class="mg-r-70 mg-t-10">
                    <a href="{{ route('page-gallerie.edit', 1) }}">
                        page gallerie
                    </a>
                </li>
                <li class="as-nav @if(request()->routeIs('page-event*')) as-active @endif"
                    role="presentation">
                    <a href="{{ route('page-event.edit', 1) }}">
                        page évènement
                    </a>
                </li>
                <li class="as-nav @if(request()->routeIs('image-gallerie*')) as-active @endif"
                    role="presentation">
                    <a href="{{ route('image-gallerie.index') }}">
                        Galleries
                    </a>
                </li>
                <li class="as-nav @if(request()->routeIs('cause*')) as-active @endif"
                    role="presentation">
                    <a href="{{ route('cause.index') }}">
                        Causes
                    </a>
                </li>
                <li class="as-nav @if(request()->routeIs('post*')) as-active @endif"
                    role="presentation">
                    <a href="{{ route('post.index') }}">
                        Mon Blog
                    </a>
                </li>
                <li class="as-nav @if(request()->routeIs('don*')) as-active @endif"
                    role="presentation">
                    <a href="{{ route('don.index') }}">
                        Don
                    </a>
                </li>
                <li class="as-nav @if(request()->routeIs('evenement*')) as-active @endif"
                    role="presentation">
                    <a href="{{ route('evenement.index') }}">
                        Evènement
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
