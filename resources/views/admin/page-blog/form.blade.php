<div class="form-group {{ $errors->has('slide') ? 'has-error' : ''}}">
    <label for="slide" class="control-label">{{ 'Slide' }}</label>
    <input class="form-control" name="slide" type="file" id="slide" value="{{ isset($pageblog->slide) ? $pageblog->slide : ''}}" >
    {!! $errors->first('slide', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('slide_title') ? 'has-error' : ''}}">
    <label for="slide_title" class="control-label">{{ 'Slide Title' }}</label>
    <input class="form-control" name="slide_title" type="text" id="slide_title" value="{{ isset($pageblog->slide_title) ? $pageblog->slide_title : ''}}" >
    {!! $errors->first('slide_title', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
