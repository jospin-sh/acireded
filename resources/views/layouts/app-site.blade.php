<!DOCTYPE html>
<html lang="fr">
<head>
    {!! Meta::toHtml() !!}
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="{{asset('images/logo_acireded.jpg')}}">
    <link href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Overpass:300,400,400i,600,700" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('vendor/css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/css/animate.css')}}">

    <link rel="stylesheet" href="{{asset('vendor/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{asset('vendor/css/aos.css')}}">

    <link rel="stylesheet" href="{{asset('vendor/css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{asset('vendor/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/css/jquery.timepicker.css')}}">


    <link rel="stylesheet" href="{{asset('vendor/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/css/icomoon.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/css/style.css')}}">

    <link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="{{route('home')}}">
            <img class="logo" src="{{asset('images/logo_acireded.jpg')}}" title="logo">ACIREDED
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item @if(request()->routeIs('home*')) active @endif"><a href="{!! route('home') !!}"
                                                                                       class="nav-link">Accueil</a></li>
                <li class="nav-item @if(request()->routeIs('about*')) active @endif"><a href="{!! route('about') !!}"
                                                                                        class="nav-link">A propos</a>
                </li>
                <li class="nav-item @if(request()->routeIs('causes*')) active @endif"><a href="{!! route('causes') !!}"
                                                                                         class="nav-link">Causes</a>
                </li>
                <li class="nav-item @if(request()->routeIs('don*')) active @endif"><a href="{!! route('don') !!}"
                                                                                         class="nav-link">Donner</a>
                </li-->
                <li class="nav-item @if(request()->routeIs('blog*')) active @endif"><a href="{!! route('blog') !!}"
                                                                                       class="nav-link">Blog</a></li>
                <li class="nav-item @if(request()->routeIs('gallery*')) active @endif"><a
                            href="{!! route('gallery') !!}" class="nav-link">Gallerie</a></li>
                <li class="nav-item @if(request()->routeIs('event*')) active @endif"><a href="{!! route('event') !!}"
                                                                                        class="nav-link">Evénements</a>
                </li>
                <li class="nav-item @if(request()->routeIs('contact*')) active @endif"><a
                            href="{!! route('contact') !!}" class="nav-link">Contact</a></li>
            </ul>
        </div>
    </div>
</nav>
<!-- END nav -->

<div class="hero-wrap" style="background-image: url(@yield('slide-image'));" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
            <div class="col-md-7 ftco-animate text-center" data-scrollax=" properties: { translateY: '70%' }">
                @hasSection('breadcrumb')
                    <p class="breadcrumbs" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span
                                class="mr-2">
                            <a href="index.html">Home</a></span> <span>@yield('bread-page')</span>
                    </p>
                @endif
                <h1 class="mb-4" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">@yield('title')</h1>
                @hasSection('watch-video')
                    <p data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><a
                                href="https://www.youtube.com/watch?v=Gb_2JOdiuZs"
                                class="btn btn-white btn-outline-white px-4 py-3 popup-vimeo"><span
                                    class="icon-play mr-2"></span>Regarder la vidéo</a></p>
                @endif
            </div>
        </div>
    </div>
</div>

<section>
    @yield('content')
</section>


<footer class="ftco-footer ftco-section img">
    <div class="overlay"></div>
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-3">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">A Propos</h2>
                    <p>ACIREDED est une association à but non lucratif qui s'est donné pour mission d'oeuvrer
                        pour un monde baignant dans la Paix et la sécurité, sans faim, sans conflits ni violences dans
                        une égalité parfaite entre hommes et femmes.</p>
                    <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                        <li class="ftco-animate"><a href="https://www.twitter.com/Acireded_Cameroun"><span class="icon-twitter"></span></a></li>
                        <li class="ftco-animate"><a href="https://www.facebook.com/Acireded_Cameroun"><span class="icon-facebook"></span></a></li>
                        <li class="ftco-animate"><a href="https://www.instagram.com/Acireded_Cameroun"><span class="icon-instagram"></span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">Récent sur le Blog</h2>
                    @foreach($posts as $post)
                        <div class="block-21 mb-4 d-flex">
                            <a class="blog-img mr-4" style="background-image: url({{asset('storage/'.$post->image)}});"></a>
                            <div class="text">
                                <h3 class="heading"><a href="#">{{$post->titre}}</a>
                                </h3>
                                <div class="meta">
                                    <div><a href="#"><span class="icon-calendar"></span>{{$post->date}}</a></div>
                                    <div><a href="#"><span class="icon-person"></span> Admin</a></div>
                                    <div><a href="#"><span class="icon-chat"></span> 19</a></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-2">
                <div class="ftco-footer-widget mb-4 ml-md-4">
                    <h2 class="ftco-heading-2">Menu</h2>
                    <ul class="list-unstyled">
                        <li><a href="#" class="py-2 d-block">Accueil</a></li>
                        <li><a href="#" class="py-2 d-block">A propos</a></li>
                        <li><a href="#" class="py-2 d-block">Donner</a></li>
                        <li><a href="#" class="py-2 d-block">Causes</a></li>
                        <li><a href="#" class="py-2 d-block">Evénements</a></li>
                        <li><a href="#" class="py-2 d-block">Blog</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">Avez-vous une question?</h2>
                    <div class="block-23 mb-3">
                        <ul>
                            <li><span class="icon icon-map-marker"></span><span class="text">Maroua-Pitoaré, dans les locaux de la délégation Régionale des Mines et de l’Industrie Technologique</span>
                            </li>
                            <li><a href="#"><span class="icon icon-phone"></span><span
                                            class="text">(+237) 693119117/68786818/693693627/696849844</span></a></li>
                            <li><a href="#"><span class="icon icon-envelope"></span>
                                    <span class="text">contact@acireded.com</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">

                <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script>
                    Tous droits réservés | Ce template a été fait avec <i class="icon-heart" aria-hidden="true"></i> par
                    <a
                            href="https://colorlib.com" target="_blank">Colorlib</a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                <p><a href="http://devpassionacademy.com" target="_blank" title="lien dev passion acamedy">
                        Réalisation: Dev Passion Academy</a></p>
            </div>
        </div>
    </div>
</footer>


<!-- loader -->
<div id="ftco-loader" class="show fullscreen">
    <svg class="circular" width="48px" height="48px">
        <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/>
        <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                stroke="#F96D00"/>
    </svg>
</div>


<script src="{{asset('vendor/js/jquery.min.js')}}"></script>
<script src="{{asset('vendor/js/jquery-migrate-3.0.1.min.js')}}"></script>
<script src="{{asset('vendor/js/popper.min.js')}}"></script>
<script src="{{asset('vendor/js/bootstrap.min.js')}}"></script>
<script src="{{asset('vendor/js/jquery.easing.1.3.js')}}"></script>
<script src="{{asset('vendor/js/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('vendor/js/jquery.stellar.min.js')}}"></script>
<script src="{{asset('vendor/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('vendor/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('vendor/js/aos.js')}}"></script>
<script src="{{asset('vendor/js/jquery.animateNumber.min.js')}}"></script>
<script src="{{asset('vendor/js/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('vendor/js/jquery.timepicker.min.js')}}"></script>
<script src="{{asset('vendor/js/scrollax.min.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
<script src="{{asset('vendor/js/google-map.js')}}"></script>
<script src="{{asset('vendor/js/main.js')}}"></script>

</body>
</html>