@extends("layouts.app-site")
@section('slide-image')
    @if(isset($page->slide)) {{asset('storage/'.$page->slide)}} @else 'images/bg_6.jpg' @endif
@endsection
@section('breadcrumb').
@parent
@section('bread-page')Donate  @endsection
@endsection
@section('title')
    @if(isset($page->slide_title)) {{$page->slide_title}} @else Donations @endif
@endsection
@section('content')
    <section class="ftco-section bg-light">
        <div class="container">
            <div class="row">
                @if(isset($dons) && !empty($don))
                    @foreach($donations as $don)
                        <div class="col-lg-4 d-flex mb-sm-4 ftco-animate">
                            <div class="staff">
                                <div class="d-flex mb-4">
                                    <div class="img" style="background-image: url({{asset($don->image)}});"></div>
                                    <div class="info ml-4">
                                        <h3><a href="teacher-single.html">{{$don->nom}}</a></h3>
                                        <span class="position">A donné le {{$don->date}}</span>
                                        <div class="text">
                                            <p>A donné <span>{{$don->montant}}</span> pour <a href="#">{{$don->cause}}</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="col-lg-4 d-flex mb-sm-4 ftco-animate">
                        <div class="staff">
                            <div class="d-flex mb-4">
                                <div class="img" style="background-image: url(images/person_1.jpg);"></div>
                                <div class="info ml-4">
                                    <h3><a href="teacher-single.html">Ivan Jacobson</a></h3>
                                    <span class="position">Don perçu il y'a quelques temps</span>
                                    <div class="text">
                                        <p>A donné <span>$300</span> pour <a href="#">La cause des enfants</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 d-flex mb-sm-4 ftco-animate">
                        <div class="staff">
                            <div class="d-flex mb-4">
                                <div class="img" style="background-image: url(images/person_2.jpg);"></div>
                                <div class="info ml-4">
                                    <h3><a href="teacher-single.html">Ivan Jacobson</a></h3>
                                    <span class="position">Don perçu il y'a quelques temps</span>
                                    <div class="text">
                                        <p>Donated <span>$150</span> for <a href="#">Children Needs Food</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 d-flex mb-sm-4 ftco-animate">
                        <div class="staff">
                            <div class="d-flex mb-4">
                                <div class="img" style="background-image: url(images/person_3.jpg);"></div>
                                <div class="info ml-4">
                                    <h3><a href="teacher-single.html">Ivan Jacobson</a></h3>
                                    <span class="position">Don perçu il y'a quelques temps</span>
                                    <div class="text">
                                        <p>Donated <span>$250</span> for <a href="#">Children Needs Food</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 d-flex mb-sm-4 ftco-animate">
                        <div class="staff">
                            <div class="d-flex mb-4">
                                <div class="img" style="background-image: url(images/person_4.jpg);"></div>
                                <div class="info ml-4">
                                    <h3><a href="teacher-single.html">Ivan Jacobson</a></h3>
                                    <span class="position">Don perçu il y'a quelques temps</span>
                                    <div class="text">
                                        <p>A donné <span>$300</span> pour <a href="#">La cause des enfants</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 d-flex mb-sm-4 ftco-animate">
                        <div class="staff">
                            <div class="d-flex mb-4">
                                <div class="img" style="background-image: url(images/person_5.jpg);"></div>
                                <div class="info ml-4">
                                    <h3><a href="teacher-single.html">Ivan Jacobson</a></h3>
                                    <span class="position">Don perçu il y'a quelques temps</span>
                                    <div class="text">
                                        <p>Donated <span>$150</span> for <a href="#">Children Needs Food</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 d-flex mb-sm-4 ftco-animate">
                        <div class="staff">
                            <div class="d-flex mb-4">
                                <div class="img" style="background-image: url(images/person_6.jpg);"></div>
                                <div class="info ml-4">
                                    <h3><a href="teacher-single.html">Ivan Jacobson</a></h3>
                                    <span class="position">Don perçu il y'a quelques temps</span>
                                    <div class="text">
                                        <p>Donated <span>$250</span> for <a href="#">Children Needs Food</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 d-flex mb-sm-4 ftco-animate">
                        <div class="staff">
                            <div class="d-flex mb-4">
                                <div class="img" style="background-image: url(images/person_7.jpg);"></div>
                                <div class="info ml-4">
                                    <h3><a href="teacher-single.html">Ivan Jacobson</a></h3>
                                    <span class="position">Don perçu il y'a quelques temps</span>
                                    <div class="text">
                                        <p>A donné <span>$300</span> pour <a href="#">La cause des enfants</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 d-flex mb-sm-4 ftco-animate">
                        <div class="staff">
                            <div class="d-flex mb-4">
                                <div class="img" style="background-image: url(images/person_8.jpg);"></div>
                                <div class="info ml-4">
                                    <h3><a href="teacher-single.html">Ivan Jacobson</a></h3>
                                    <span class="position">Don perçu il y'a quelques temps</span>
                                    <div class="text">
                                        <p>Donated <span>$150</span> for <a href="#">Children Needs Food</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 d-flex mb-sm-4 ftco-animate">
                        <div class="staff">
                            <div class="d-flex mb-4">
                                <div class="img" style="background-image: url(images/person_9.jpg);"></div>
                                <div class="info ml-4">
                                    <h3><a href="teacher-single.html">Ivan Jacobson</a></h3>
                                    <span class="position">Don perçu il y'a quelques temps</span>
                                    <div class="text">
                                        <p>Donated <span>$250</span> for <a href="#">Children Needs Food</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <div class="row mt-5">
                <div class="col text-center">
                    <div class="block-27">
                        <ul>
                            <li><a href="#">&lt;</a></li>
                            <li class="active"><span>1</span></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">&gt;</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section-3 img" style="background-image: url({{asset('images/bg_4.jpg')}});">
        <div class="overlay"></div>
        <div class="container">
            <div class="row d-md-flex">
                <div class="col-md-6 d-flex ftco-animate">
                    <div class="img img-2 align-self-stretch" style="background-image: url({{asset('images/bg_4.jpg')}});"></div>
                </div>
                <div class="col-md-6 volunteer pl-md-5 ftco-animate">
                    <h3 class="mb-3">Devenez volontaire</h3>
                    <form action="#" class="volunter-form">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Votre nom">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Votre Email">
                        </div>
                        <div class="form-group">
                            <textarea name="" id="" cols="30" rows="3" class="form-control"
                                      placeholder="Message"></textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Envoyer" class="btn btn-white py-3 px-5">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection