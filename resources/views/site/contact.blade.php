@extends("layouts.app-site")
@section('slide-image')
    @if(isset($page->slide)) {{asset('storage/'.$page->$slide)}} @else 'images/bg_2.jpg' @endif
@endsection
@section('breadcrumb').
@parent
@section('bread-page')Contact  @endsection
@endsection
@section('title')
    @if(isset($page->slide_title)) {{$page->slide_title}} @else Contact @endif
@endsection
@section('content')
    <section class="ftco-section contact-section ftco-degree-bg" xmlns="http://www.w3.org/1999/html">
        <div class="container">
            @component('helpers.alert')
                .
            @endcomponent
            <div class="row d-flex mb-5 contact-info">
                <div class="col-md-12 mb-4">
                    <h2 class="h4">Information de Contact</h2>
                @if(isset($site) && !empty($site))
                    <div class="w-100"></div>
                    <div class="col-md-3">
                        <p><span>Adresse:</span> {{$site->adresse}}</p>
                    </div>
                    <div class="col-md-3">
                        <p><span>Phone:</span> <a href="tel://1234567920">{{$site->telephone}}</a></p>
                    </div>
                    <div class="col-md-3">
                        <p><span>Email:</span> <a href="mailto:info@yoursite.com">{{$site->email}}</a></p>
                    </div>
                    <div class="col-md-3">
                        <p><span>Site web</span> <a href="{{route('home')}}" >{{route('home')}}</a></p>
                    </div>
                @else
                    <div class="w-100"></div>
                    <div class="row">
                    <div class="col-md-3">
                        <p><span>Adresse:</span> Maroua-Pitoaré, dans les locaux de la délégation Régionale des Mines et de l’Industrie Technologique</p>
                    </div>
                    <div class="col-md-3">
                        <p><span>Téléphone:</span> <a href="tel://+237693119117">(+237) 693119117/68786818/693693627/696849844</a></p>
                        <p><span>Email:</span> <a href="mailto:contact@acireded.com">contact@acireded.com</a></p>
                    </div>
                    </div>
                @endif
                <br/>
                <div class="row block-9">
                    <div class="col-md-6 pr-md-5">
                        <h4 class="mb-4">Laissez nous un message.</h4>
                        <form action="{!! route('contactus') !!}" method="post">
                            @csrf
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" placeholder="Votre Nom" required>
                            </div>
                            <div class="form-group">
                                <input type="text" name="email" class="form-control" placeholder="Votre Email" required>
                            </div>
                            <div class="form-group">
                                <input type="text" name="subject" class="form-control" placeholder="Objet" required>
                            </div>
                            <div class="form-group">
                            <textarea id="" name="message" cols="30" rows="7" class="form-control"
                                      placeholder="Message" required></textarea>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Envoyer" class="btn btn-primary py-3 px-5">
                            </div>
                        </form>

                    </div>

                    <div class="col-md-6" id="map"></div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection