@extends("layouts.app-site")
@section('slide-image')
    @if(isset($page->slide)) {{asset('storage/'.$page->slide)}} @else 'images/bg_5.jpg' @endif
@endsection
@section('breadcrumb').
@parent
@section('bread-page') Causes  @endsection
@endsection
@section('title')
    @if(isset($page->slide_title)) {{$page->slide_title}} @else Causes @endif
@endsection
@section('content')
    <section class="ftco-section">
        <div class="container">
            <div class="row">
                @if(isset($causes) && !empty($causes))
                    @foreach($causes as $cause)
                        <div class="col-md-4 ftco-animate">
                            <div class="cause-entry">
                                <a href="#" class="img" style="background-image: url({{asset('storage/'.$cause->image)}});"></a>
                                <div class="text p-3 p-md-4">
                                    <h3><a href="#">{{$cause->titre}}</a></h3>
                                    <p class="preview-content">{{$cause->description}}</p>
                                    <span class="donation-time mb-3 d-block">Last donation 1w ago</span>
                                    <div class="progress custom-progress-success">
                                        <div class="progress-bar bg-primary" role="progressbar"
                                             style="width: 28%" aria-valuenow="28" aria-valuemin="0"
                                             aria-valuemax="100"></div>
                                    </div>
                                    <span class="fund-raised d-block">$12,000 raised of $30,000</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="col-md-4 ftco-animate">
                        <div class="cause-entry">
                            <a href="#" class="img" style="background-image: url({{asset('images/cause-1.jpg')}});"></a>
                            <div class="text p-3 p-md-4">
                                <h3><a href="#">Clean water for the urban area</a></h3>
                                <p>Even the all-powerful Pointing has no control about the blind texts it is an almost
                                    unorthographic life</p>
                                <span class="donation-time mb-3 d-block">Last donation 1w ago</span>
                                <div class="progress custom-progress-success">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 28%"
                                         aria-valuenow="28" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="fund-raised d-block">$12,000 raised of $30,000</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 ftco-animate">
                        <div class="cause-entry">
                            <a href="#" class="img" style="background-image: url({{asset('images/cause-2.jpg')}});"></a>
                            <div class="text p-3 p-md-4">
                                <h3><a href="#">Clean water for the urban area</a></h3>
                                <p>Even the all-powerful Pointing has no control about the blind texts it is an almost
                                    unorthographic life</p>
                                <span class="donation-time mb-3 d-block">Last donation 1w ago</span>
                                <div class="progress custom-progress-success">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 28%"
                                         aria-valuenow="28" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="fund-raised d-block">$12,000 raised of $30,000</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 ftco-animate">
                        <div class="cause-entry">
                            <a href="#" class="img" style="background-image: url({{asset('images/cause-3.jpg')}});"></a>
                            <div class="text p-3 p-md-4">
                                <h3><a href="#">Clean water for the urban area</a></h3>
                                <p>Even the all-powerful Pointing has no control about the blind texts it is an almost
                                    unorthographic life</p>
                                <span class="donation-time mb-3 d-block">Last donation 1w ago</span>
                                <div class="progress custom-progress-success">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 28%"
                                         aria-valuenow="28" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="fund-raised d-block">$12,000 raised of $30,000</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 ftco-animate">
                        <div class="cause-entry">
                            <a href="#" class="img" style="background-image: url({{asset('images/cause-4.jpg')}});"></a>
                            <div class="text p-3 p-md-4">
                                <h3><a href="#">Clean water for the urban area</a></h3>
                                <p>Even the all-powerful Pointing has no control about the blind texts it is an almost
                                    unorthographic life</p>
                                <span class="donation-time mb-3 d-block">Last donation 1w ago</span>
                                <div class="progress custom-progress-success">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 28%"
                                         aria-valuenow="28" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="fund-raised d-block">$12,000 raised of $30,000</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 ftco-animate">
                        <div class="cause-entry">
                            <a href="#" class="img" style="background-image: url({{asset('images/cause-5.jpg')}});"></a>
                            <div class="text p-3 p-md-4">
                                <h3><a href="#">Clean water for the urban area</a></h3>
                                <p>Even the all-powerful Pointing has no control about the blind texts it is an almost
                                    unorthographic life</p>
                                <span class="donation-time mb-3 d-block">Last donation 1w ago</span>
                                <div class="progress custom-progress-success">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 28%"
                                         aria-valuenow="28" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="fund-raised d-block">$12,000 raised of $30,000</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 ftco-animate">
                        <div class="cause-entry">
                            <a href="#" class="img" style="background-image: url({{asset('images/cause-6.jpg')}});"></a>
                            <div class="text p-3 p-md-4">
                                <h3><a href="#">Clean water for the urban area</a></h3>
                                <p>Even the all-powerful Pointing has no control about the blind texts it is an almost
                                    unorthographic life</p>
                                <span class="donation-time mb-3 d-block">Last donation 1w ago</span>
                                <div class="progress custom-progress-success">
                                    <div class="progress-bar bg-primary" role="progressbar" style="width: 28%"
                                         aria-valuenow="28" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="fund-raised d-block">$12,000 raised of $30,000</span>
                            </div>
                        </div>
                    </div>
                @endif

            </div>
            <div class="row mt-5">
                <div class="col text-center">
                    <div class="block-27">
                        <ul>
                            <li><a href="#">&lt;</a></li>
                            <li class="active"><span>1</span></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">&gt;</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section-3 img" style="background-image: url({{asset('images/bg_4.jpg')}});">
        <div class="overlay"></div>
        <div class="container">
            <div class="row d-md-flex">
                <div class="col-md-6 d-flex ftco-animate">
                    <div class="img img-2 align-self-stretch" style="background-image: url({{asset('images/bg_4.jpg')}});"></div>
                </div>
                <div class="col-md-6 volunteer pl-md-5 ftco-animate">
                    <h3 class="mb-3">Devenez volontaire</h3>
                    <form action="{!! route('contact') !!}" method="post" class="volunter-form">
                        @csrf
                        <div class="form-group">
                            <input type="hidden" name="subject" id="subject" value="Devenir volontaire" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="text" name="name" id="name" class="form-control" placeholder="Votre nom" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="email" id="email" class="form-control" placeholder="Votre Email" required>
                        </div>
                        <div class="form-group">
                            <textarea name="message" id="message" cols="30" rows="3" class="form-control"
                                      placeholder="Message" required></textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Envoyer" class="btn btn-white py-3 px-5">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection