@extends("layouts.app-site")
@section('slide-image')
    @if(isset($page->slide)) {{asset('storage/'.$page->slide)}} @else 'images/bg_1.jpg' @endif
@endsection
@section('breadcrumb').
@parent
@section('bread-page')Event  @endsection
@endsection
@section('title')
    @if(isset($page->slide_title)) {{$page->slide_title}} @else Events @endif
@endsection
@section('content')
    <section class="ftco-section">
        <div class="container">
            <div class="row">
                @if(isset($events) && !empty($events))
                    @foreach($events as $event)
                        <div class="col-md-4 d-flex ftco-animate">
                            <div class="blog-entry align-self-stretch">
                                <a href="blog-single.html" class="block-20"
                                   style="background-image: url({{ asset('storage/'.$event->image) }});">
                                </a>
                                <div class="text p-4 d-block">
                                    <div class="meta mb-3">
                                        <div><a href="#">{{$event->date}}</a></div>
                                        <div><a href="#">Admin</a></div>
                                        <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                                    </div>
                                    <h3 class="heading mb-4"><a href="#">{{$event->titre}}</a></h3>
                                    <p class="time-loc"><span class="mr-2"><i
                                                    class="icon-clock-o"></i> {{$event->heure_debut}}</span> <span><i
                                                    class="icon-map-o"></i> {{$event->lieu}}</span></p>
                                    <p class="preview-content">{{$event->description}}</p>
                                    <p><a href="event.html">Voir plus <i class="ion-ios-arrow-forward"></i></a></p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="blog-entry align-self-stretch">
                            <a href="blog-single.html" class="block-20"
                               style="background-image: url('images/event-1.jpg');">
                            </a>
                            <div class="text p-4 d-block">
                                <div class="meta mb-3">
                                    <div><a href="#">Sep. 10, 2018</a></div>
                                    <div><a href="#">Admin</a></div>
                                    <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                                </div>
                                <h3 class="heading mb-4"><a href="#">World Wide Donation</a></h3>
                                <p class="time-loc"><span class="mr-2"><i
                                                class="icon-clock-o"></i> 10:30AM-03:30PM</span> <span><i
                                                class="icon-map-o"></i> Venue Main Campus</span></p>
                                <p>A small river named Duden flows by their place and supplies it with the necessary
                                    regelialia.</p>
                                <p><a href="event.html">Join Event <i class="ion-ios-arrow-forward"></i></a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="blog-entry align-self-stretch">
                            <a href="blog-single.html" class="block-20"
                               style="background-image: url('images/event-2.jpg');">
                            </a>
                            <div class="text p-4 d-block">
                                <div class="meta mb-3">
                                    <div><a href="#">Sep. 10, 2018</a></div>
                                    <div><a href="#">Admin</a></div>
                                    <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                                </div>
                                <h3 class="heading mb-4"><a href="#">World Wide Donation</a></h3>
                                <p class="time-loc"><span class="mr-2"><i
                                                class="icon-clock-o"></i> 10:30AM-03:30PM</span> <span><i
                                                class="icon-map-o"></i> Venue Main Campus</span></p>
                                <p>A small river named Duden flows by their place and supplies it with the necessary
                                    regelialia.</p>
                                <p><a href="event.html">Join Event <i class="ion-ios-arrow-forward"></i></a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="blog-entry align-self-stretch">
                            <a href="blog-single.html" class="block-20"
                               style="background-image: url('images/event-3.jpg');">
                            </a>
                            <div class="text p-4 d-block">
                                <div class="meta mb-3">
                                    <div><a href="#">Sep. 10, 2018</a></div>
                                    <div><a href="#">Admin</a></div>
                                    <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                                </div>
                                <h3 class="heading mb-4"><a href="#">World Wide Donation</a></h3>
                                <p class="time-loc"><span class="mr-2"><i
                                                class="icon-clock-o"></i> 10:30AM-03:30PM</span> <span><i
                                                class="icon-map-o"></i> Venue Main Campus</span></p>
                                <p>A small river named Duden flows by their place and supplies it with the necessary
                                    regelialia.</p>
                                <p><a href="event.html">Join Event <i class="ion-ios-arrow-forward"></i></a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="blog-entry align-self-stretch">
                            <a href="blog-single.html" class="block-20"
                               style="background-image: url('images/event-4.jpg');">
                            </a>
                            <div class="text p-4 d-block">
                                <div class="meta mb-3">
                                    <div><a href="#">Sep. 10, 2018</a></div>
                                    <div><a href="#">Admin</a></div>
                                    <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                                </div>
                                <h3 class="heading mb-4"><a href="#">World Wide Donation</a></h3>
                                <p class="time-loc"><span class="mr-2"><i
                                                class="icon-clock-o"></i> 10:30AM-03:30PM</span> <span><i
                                                class="icon-map-o"></i> Venue Main Campus</span></p>
                                <p>A small river named Duden flows by their place and supplies it with the necessary
                                    regelialia.</p>
                                <p><a href="event.html">Join Event <i class="ion-ios-arrow-forward"></i></a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="blog-entry align-self-stretch">
                            <a href="blog-single.html" class="block-20"
                               style="background-image: url('images/event-5.jpg');">
                            </a>
                            <div class="text p-4 d-block">
                                <div class="meta mb-3">
                                    <div><a href="#">Sep. 10, 2018</a></div>
                                    <div><a href="#">Admin</a></div>
                                    <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                                </div>
                                <h3 class="heading mb-4"><a href="#">World Wide Donation</a></h3>
                                <p class="time-loc"><span class="mr-2"><i
                                                class="icon-clock-o"></i> 10:30AM-03:30PM</span> <span><i
                                                class="icon-map-o"></i> Venue Main Campus</span></p>
                                <p>A small river named Duden flows by their place and supplies it with the necessary
                                    regelialia.</p>
                                <p><a href="event.html">Join Event <i class="ion-ios-arrow-forward"></i></a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="blog-entry align-self-stretch">
                            <a href="blog-single.html" class="block-20"
                               style="background-image: url('images/event-6.jpg');">
                            </a>
                            <div class="text p-4 d-block">
                                <div class="meta mb-3">
                                    <div><a href="#">Sep. 10, 2018</a></div>
                                    <div><a href="#">Admin</a></div>
                                    <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                                </div>
                                <h3 class="heading mb-4"><a href="#">World Wide Donation</a></h3>
                                <p class="time-loc"><span class="mr-2"><i
                                                class="icon-clock-o"></i> 10:30AM-03:30PM</span> <span><i
                                                class="icon-map-o"></i> Venue Main Campus</span></p>
                                <p>A small river named Duden flows by their place and supplies it with the necessary
                                    regelialia.</p>
                                <p><a href="event.html">Join Event <i class="ion-ios-arrow-forward"></i></a></p>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <div class="row mt-5">
                <div class="col text-center">
                    <div class="block-27">
                        <ul>
                            <li><a href="#">&lt;</a></li>
                            <li class="active"><span>1</span></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">&gt;</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection