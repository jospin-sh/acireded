@extends("layouts.app-site")
@section('slide-image')
    @if(isset($page->slide)) {{asset('storage/'.$page->slide)}} @else 'images/bg_6.jpg' @endif
@endsection
@section('breadcrumb').
@parent
@section('bread-page')Donate  @endsection
@endsection
@section('title')
    @if(isset($page->slide_title)) {{$page->slide_title}} @else Donations @endif
@endsection
@section('content')
    <section class="ftco-section bg-light">
        <div class="container">
            <h3>Faites un don par ces moyens de paiement</h3>
            <div class="row">
                <div class="col-md-6 pay-mode"><h5>Espèce</h5>
                    Vous pouvez faire un don pour soutenir notre cause par espèce. Pour cela, contactez-nous au (+237) 693119117 ou (+237) 693693627
                    ou par email à l'adresse contact@acireded.com
                </div>
                <div class="col-md-6 pay-mode"><h5>Virement bancaire</h5>
                    Vous pouvez faire un don par virement / dépôt  bancaire espèce dans notre compte XXX
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-6 pay-mode"><h5>Orange MONEY</h5>
                    Vous pouvez faire un don pour soutenir notre cause par Orange Money au numéro XXX
                </div>
                <div class="col-md-6 pay-mode"><h5>MTN Mobile Money</h5>
                    Vous pouvez faire un don pour soutenir notre cause par MTN Mobile Money au numéro XXX
                </div>
                <br/>
            </div>
            <br/>
        </div>
    </section>
@endsection