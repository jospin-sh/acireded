@extends("layouts.app-site")
@section('slide-image')

    @if(isset($page->slide))  {{asset('storage/'.$page->slide)}} @else 'images/bg_7.jpg' @endif
@endsection
@section('breadcrumb').
    @parent
    @section('bread-page')
        About
    @endsection
@endsection
@section('title')
    @if(isset($page->slide_title)) {{$page->slide_title}} @else About us @endif
@endsection
@section('content')
    <!---- page content -------------------------------------------------------------------------------------->
    <section class="ftco-section">
        @if(isset($page->image) && isset($page->description))
            <div class="container">
                <div class="row d-flex">
                    <div class="col-md-6 d-flex ftco-animate">
                        <div class="img img-about align-self-stretch" style="background-image: url({{asset("storage/".$page->image)}}); width: 100%; border: 1px solid
                                #80808033; border-radius: 10px; height: 400px;"></div>
                    </div>
                    <div class="col-md-6 pl-md-5 ftco-animate">
                        <h2 class="mb-4">{{$page->titre}}</h2>
                        <p>{{$page->description}}</p>
                    </div>
            </div>
         @else
            <div class="row d-flex">
                <div class="col-md-6 d-flex ftco-animate">
                    <div class="img img-about align-self-stretch" style="background-image: url({{asset('images/bg_3.jpg')}}); width: 100%;"></div>
                </div>
                <div class="col-md-6 pl-md-5 ftco-animate">
                    <h2 class="mb-4">Welcome to Welfare Stablished Since 1898</h2>
                    <p>The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
                    <p>On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word "and" and the Little Blind Text should turn around and return to its own, safe country. But nothing the copy said could convince her and so it didn’t take long until a few insidious Copy Writers ambushed her, made her drunk with Longe and Parole and dragged her into their agency, where they abused her for their.</p>
                </div>
            </div>
         @endif
        </div>
    </section>

    <section class="ftco-counter ftco-intro" id="section-counter">
        <div class="container">
            <div class="row no-gutters">
                <div class="col-md-5 d-flex justify-content-center counter-wrap ftco-animate">
                    <div class="block-18 color-1 align-items-stretch">
                        <div class="text">
                            <span>Nous avons servi plus de </span>
                            <strong class="number" data-number="1432805">0</strong>
                            <span>Enfants dans le monde.</span>
                        </div>
                    </div>
                </div>
                <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                    <div class="block-18 color-2 align-items-stretch">
                        <div class="text">
                            <h3 class="mb-4">Faire un don</h3>
                            <p>Aidez-nous à leur venir en aide, faites un don.</p>
                            <p><a href="{!! route('don') !!}" class="btn btn-white px-3 py-2 mt-2">Donner maintenant</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                    <div class="block-18 color-3 align-items-stretch">
                        <div class="text">
                            <h3 class="mb-4">Devenez volontaire</h3>
                            <p>Rejoingnez une équipe dévouée à la cause des enfants et des jeunes.</p>
                            <p><a href="{!! route('home') !!}#contact" class="btn btn-white px-3 py-2 mt-2">Devenez un volontaire</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section bg-light">
        <div class="container">
            <div class="row justify-content-center mb-5 pb-3">
                <div class="col-md-7 heading-section ftco-animate text-center">
                    <h2 class="mb-4">Notre équipe</h2>
                    <p>Notre équipe se veut jeune, dynamique et entièrement engagée à la cause que nous soutenons.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 d-flex mb-sm-4 ftco-animate">
                    <div class="staff">
                        <div class="d-flex mb-4">
                            <div class="img" style="background-image: url({{asset('images/person_1.jpg')}});"></div>
                            <div class="info ml-4">
                                <h3><a href="teacher-single.html">SITCHEU WANKO Sorelle</a></h3>
                                <span class="position">Présidente-Fondatrice</span>
                                <div class="text">
                                    <p>Elle assure la direction de l’Association et la représente dans tous les actes de la vie publique
                                        et privée.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 d-flex mb-sm-4 ftco-animate">
                    <div class="staff">
                        <div class="d-flex mb-4">
                            <div class="img" style="background-image: url({{asset('images00/person_2.jpg')}});"></div>
                            <div class="info ml-4">
                                <h3><a href="teacher-single.html">POUTONG Rocard</a></h3>
                                <span class="position">Président Adjoint</span>
                                <div class="text">
                                    <p>Il assiste la présidente dans ses fonctions. Il reçoit à cet effet les délégations de pouvoir.
                                        </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 d-flex mb-sm-4 ftco-animate">
                    <div class="staff">
                        <div class="d-flex mb-4">
                            <div class="img" style="background-image: url({{asset('images/person_3.jpg')}});"></div>
                            <div class="info ml-4">
                                <h3><a href="teacher-single.html">KUICHOU Beaudoit Merlin</a></h3>
                                <span class="position">Secrétaire général</span>
                                <p>Il assure le secrétariat lors des réunions du Bureau exécutif et de l’Assemblée générale.
                                    Il tient à jour tous les dossiers de l’Association.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection