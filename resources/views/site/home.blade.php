@extends("layouts.app-site")
@section('slide-image')
    @if(isset($page->slide)) {{asset('storage/'.$page->slide)}} @else 'images/bg_7.jpg' @endif
@endsection
@section('title')
    @if(isset($page->slide_title)) {{$page->slide_title}} @else Doing Nothing is Not An Option of Our Life @endif
@endsection
@section('watch-video').@endsection
@section('content')
    <!-- Home page content ----------------------------------------------------------------------------------------------------------------------->
    <section class="ftco-counter ftco-intro" id="section-counter">
        <div class="container">
            <div class="row no-gutters">
                <div class="col-md-5 d-flex justify-content-center counter-wrap ftco-animate">
                    <div class="block-18 color-1 align-items-stretch">
                        <div class="text">
                            <span>Nous avons servi plus de </span>
                            <strong class="number" data-number="1432805">0</strong>
                            <span>Enfants dans le monde.</span>
                        </div>
                    </div>
                </div>
                <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                    <div class="block-18 color-2 align-items-stretch">
                        <div class="text">
                            <h3 class="mb-4">Faire un don</h3>
                            <p>Aidez-nous à leur venir en aide, faites un don.</p>
                            <p><a href="{!! route('don') !!}" class="btn btn-white px-3 py-2 mt-2">Donner maintenant</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
                    <div class="block-18 color-3 align-items-stretch">
                        <div class="text">
                            <h3 class="mb-4">Devenez volontaire</h3>
                            <p>Rejoingnez une équipe dévouée à la cause des enfants et des jeunes.</p>
                            <p><a href="#contact" class="btn btn-white px-3 py-2 mt-2">Devenez un volontaire</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="ftco-section">
        <div class="container">
            <div class="row">
                <div class="col-md-4 d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 d-flex services p-3 py-4 d-block">
                        <div class="icon d-flex mb-3"><span class="flaticon-donation-1"></span></div>
                        <div class="media-body pl-4">
                            <h3 class="heading">Faites un don</h3>
                            <p>Aidez-nous à leur venir en aide, faites un don.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 d-flex services p-3 py-4 d-block">
                        <div class="icon d-flex mb-3"><span class="flaticon-charity"></span></div>
                        <div class="media-body pl-4">
                            <h3 class="heading">Devenez un volontaire</h3>
                            <p>Rejoingnez une équipe dévoué à la cause des jeunes pour une cotoyenneté plus responsable et un monde meilleur.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 d-flex services p-3 py-4 d-block">
                        <div class="icon d-flex mb-3"><span class="flaticon-donation"></span></div>
                        <div class="media-body pl-4">
                            <h3 class="heading">Sponsoring</h3>
                            <p>Nous avons besoin de mécènes pour élargir notre champ d'action, n'hésitez pas à prendre contact avec nous.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="ftco-section bg-light">
        <div class="container-fluid">
            <div class="row justify-content-center mb-5 pb-3">
                <div class="col-md-5 heading-section ftco-animate text-center">
                    <h2 class="mb-4">Nos motivations</h2>
                    <p>Nous voulons contribuer à un monde baignant dans la Paix et la sécurité, sans faim, sans conflits ni violences.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 ftco-animate">
                    <div class="carousel-cause owl-carousel">
                        @if(isset($causes) && !empty($causes))
                            @foreach($causes as $cause)
                                <div class="item">
                                    <div class="cause-entry">
                                        <a href="#" class="img" style="background-image: url({{asset('storage/'.$cause->image)}});"></a>
                                        <div class="text p-3 p-md-4">
                                            <h3><a href="#">{{$cause->titre}}</a></h3>
                                            <p>{{$cause->description}}</p>
                                            <span class="donation-time mb-3 d-block">Dernier don il y'a 1 an</span>
                                            <div class="progress custom-progress-success">
                                                <div class="progress-bar bg-primary" role="progressbar"
                                                     style="width: 28%" aria-valuenow="28" aria-valuemin="0"
                                                     aria-valuemax="100"></div>
                                            </div>
                                            <span class="fund-raised d-block">Soutenez cette cause</span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="item">
                                <div class="cause-entry">
                                    <a href="#" class="img" style="background-image: url({{asset('images/cause-1.jpg')}});"></a>
                                    <div class="text p-3 p-md-4">
                                        <h3><a href="#">Clean water for the urban area</a></h3>
                                        <p>Even the all-powerful Pointing has no control about the blind texts it is an
                                            almost unorthographic life</p>
                                        <span class="donation-time mb-3 d-block">Last donation 1w ago</span>
                                        <div class="progress custom-progress-success">
                                            <div class="progress-bar bg-primary" role="progressbar" style="width: 28%"
                                                 aria-valuenow="28" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <span class="fund-raised d-block">Soutenez cette cause</span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="cause-entry">
                                    <a href="#" class="img" style="background-image: url({{asset('images/cause-2.jpg')}});"></a>
                                    <div class="text p-3 p-md-4">
                                        <h3><a href="#">Pour la Paix et la sécurité</a></h3>
                                        <p>Un monde baignant dans la Paix et la sécurité, sans faim, sans conflits ni violences</p>
                                        <span class="donation-time mb-3 d-block">Last donation 1w ago</span>
                                        <div class="progress custom-progress-success">
                                            <div class="progress-bar bg-primary" role="progressbar" style="width: 28%"
                                                 aria-valuenow="28" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <span class="fund-raised d-block">Soutenez cette cause</span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="cause-entry">
                                    <a href="#" class="img" style="background-image: url({{asset('images/cause-3.jpg')}});"></a>
                                    <div class="text p-3 p-md-4">
                                        <h3><a href="#">Clean water for the urban area</a></h3>
                                        <p>Even the all-powerful Pointing has no control about the blind texts it is an
                                            almost unorthographic life</p>
                                        <span class="donation-time mb-3 d-block">Last donation 1w ago</span>
                                        <div class="progress custom-progress-success">
                                            <div class="progress-bar bg-primary" role="progressbar" style="width: 28%"
                                                 aria-valuenow="28" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <span class="fund-raised d-block">$12,000 raised of $30,000</span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="cause-entry">
                                    <a href="#" class="img" style="background-image: url({{asset('images/cause-4.jpg')}});"></a>
                                    <div class="text p-3 p-md-4">
                                        <h3><a href="#">Clean water for the urban area</a></h3>
                                        <p>Even the all-powerful Pointing has no control about the blind texts it is an
                                            almost unorthographic life</p>
                                        <span class="donation-time mb-3 d-block">Last donation 1w ago</span>
                                        <div class="progress custom-progress-success">
                                            <div class="progress-bar bg-primary" role="progressbar" style="width: 28%"
                                                 aria-valuenow="28" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <span class="fund-raised d-block">$12,000 raised of $30,000</span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="cause-entry">
                                    <a href="#" class="img" style="background-image: url({{asset('images/cause-5.jpg')}});"></a>
                                    <div class="text p-3 p-md-4">
                                        <h3><a href="#">Clean water for the urban area</a></h3>
                                        <p>Even the all-powerful Pointing has no control about the blind texts it is an
                                            almost unorthographic life</p>
                                        <span class="donation-time mb-3 d-block">Last donation 1w ago</span>
                                        <div class="progress custom-progress-success">
                                            <div class="progress-bar bg-primary" role="progressbar" style="width: 28%"
                                                 aria-valuenow="28" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <span class="fund-raised d-block">$12,000 raised of $30,000</span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="cause-entry">
                                    <a href="#" class="img" style="background-image: url({{asset('images/cause-6.jpg')}});"></a>
                                    <div class="text p-3 p-md-4">
                                        <h3><a href="#">Clean water for the urban area</a></h3>
                                        <p>Even the all-powerful Pointing has no control about the blind texts it is an
                                            almost unorthographic life</p>
                                        <span class="donation-time mb-3 d-block">Last donation 1w ago</span>
                                        <div class="progress custom-progress-success">
                                            <div class="progress-bar bg-primary" role="progressbar" style="width: 28%"
                                                 aria-valuenow="28" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <span class="fund-raised d-block">$12,000 raised of $30,000</span>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--section class="ftco-section">
        <div class="container">
            <div class="row justify-content-center mb-5 pb-3">
                <div class="col-md-7 heading-section ftco-animate text-center">
                    <h2 class="mb-4">Derniers dons</h2>
                    <p>Nous recevons des dons de sources diverses, c'est cela notre force pour mener des actions d'impact de plus en plus grand..</p>
                </div>
            </div>
            @if(isset($donations) && !empty($donations))
                <div class="row">
                    @foreach($donations as $don)
                        <div class="col-lg-4 d-flex mb-sm-4 ftco-animate">
                            <div class="staff">
                                <div class="d-flex mb-4">
                                    <div class="img" style="background-image: url({{asset('images/person_1.jpg')}});"></div>
                                    <div class="info ml-4">
                                        <h3><a href="teacher-single.html">{{$don->nom}}</a></h3>
                                        <span class="position">Don perçu il y'a peu de temps</span>
                                        <div class="text">
                                            <p>A donné <span>{{$don->montant}}</span> pour <a href="#">La cause des enfants</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="row">
                    <div class="col-lg-4 d-flex mb-sm-4 ftco-animate">
                        <div class="staff">
                            <div class="d-flex mb-4">
                                <div class="img" style="background-image: url({{asset('images/person_1.jpg')}});"></div>
                                <div class="info ml-4">
                                    <h3><a href="teacher-single.html">Ivan Jacobson</a></h3>
                                    <span class="position">Don perçu il y'a peu de temps</span>
                                    <div class="text">
                                        <p>A donné <span>$300</span> pour <a href="#">La cause des enfants</</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 d-flex mb-sm-4 ftco-animate">
                        <div class="staff">
                            <div class="d-flex mb-4">
                                <div class="img" style="background-image: url({{asset('images/person_2.jpg')}});"></div>
                                <div class="info ml-4">
                                    <h3><a href="teacher-single.html">Ivan Jacobson</a></h3>
                                    <span class="position">Don perçu il y'a peu de temps</span>
                                    <div class="text">
                                        <p>A donné <span>$300</span> pour <a href="#">La cause des enfants</</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 d-flex mb-sm-4 ftco-animate">
                        <div class="staff">
                            <div class="d-flex mb-4">
                                <div class="img" style="background-image: url({{asset('images/person_3.jpg')}});"></div>
                                <div class="info ml-4">
                                    <h3><a href="teacher-single.html">Ivan Jacobson</a></h3>
                                    <span class="position">Don perçu il y'a peu de temps</span>
                                    <div class="text">
                                        <p>A donné <span>$300</span> pour <a href="#">La cause des enfants</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </section-->

    <section class="ftco-gallery">
        @if(isset($galleries) && !empty($galleries))
            @foreach($galleries as $k=>$gallery)
                @if($k % 4 == 0)
                    <div class="d-md-flex">
                        @endif
                        <a href="{{asset('storage/'.$gallery->image)}}"
                           class="gallery image-popup d-flex justify-content-center align-items-center img ftco-animate"
                           style="background-image: url({{asset('storage/'.$gallery->image)}});">
                            <div class="icon d-flex justify-content-center align-items-center">
                                <span class="icon-search"></span>
                            </div>
                        </a>
                        @if($loop->last || ($k+1) % 4 == 0)
                    </div>
                @endif
            @endforeach
        @else
            <div class="d-md-flex">
                <a href="{{asset('images/cause-2.jpg')}}"
                   class="gallery image-popup d-flex justify-content-center align-items-center img ftco-animate"
                   style="background-image: url({{asset('images/cause-2.jpg')}});">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="icon-search"></span>
                    </div>
                </a>
                <a href="images/cause-3.jpg"
                   class="gallery image-popup d-flex justify-content-center align-items-center img ftco-animate"
                   style="background-image: url({{asset('images/cause-3.jpg')}});">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="icon-search"></span>
                    </div>
                </a>
                <a href="images/cause-4.jpg"
                   class="gallery image-popup d-flex justify-content-center align-items-center img ftco-animate"
                   style="background-image: url({{asset('images/cause-4.jpg')}});">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="icon-search"></span>
                    </div>
                </a>
                <a href="images/cause-5.jpg"
                   class="gallery image-popup d-flex justify-content-center align-items-center img ftco-animate"
                   style="background-image: url({{asset('images/cause-5.jpg')}});">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="icon-search"></span>
                    </div>
                </a>
            </div>
            <div class="d-md-flex">
                <a href="images/cause-6.jpg"
                   class="gallery image-popup d-flex justify-content-center align-items-center img ftco-animate"
                   style="background-image: url({{asset('images/cause-6.jpg')}});">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="icon-search"></span>
                    </div>
                </a>
                <a href="images/image_3.jpg"
                   class="gallery image-popup d-flex justify-content-center align-items-center img ftco-animate"
                   style="background-image: url({{asset('images/image_3.jpg')}});">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="icon-search"></span>
                    </div>
                </a>
                <a href="images/image_1.jpg"
                   class="gallery image-popup d-flex justify-content-center align-items-center img ftco-animate"
                   style="background-image: url({{asset('images/image_1.jpg')}});">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="icon-search"></span>
                    </div>
                </a>
                <a href="images/image_2.jpg"
                   class="gallery image-popup d-flex justify-content-center align-items-center img ftco-animate"
                   style="background-image: url({{asset('images/image_2.jpg')}});">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="icon-search"></span>
                    </div>
                </a>
            </div>
        @endif
    </section>

    <section class="ftco-section">
        <div class="container">
            <div class="row justify-content-center mb-5 pb-3">
                <div class="col-md-7 heading-section ftco-animate text-center">
                    <h2 class="mb-4">Récents sur notre blog</h2>
                    <p>Suivez les dernières évènements de notre association sur notre blog Nous le mettons permanament à jour pour vous.</p>
                </div>
            </div>
            <div class="row d-flex">
                @if(isset($posts) && !empty($posts))
                    @foreach($posts as $post)
                        <div class="col-md-4 d-flex ftco-animate">
                            <div class="blog-entry align-self-stretch">
                                <a href="blog-single.html" class="block-20"
                                   style="background-image: url({{asset('storage/'.$post->image)}});">
                                </a>
                                <div class="text p-4 d-block">
                                    <div class="meta mb-3">
                                        <div><a href="#">{{$post->date}}</a></div>
                                        <div><a href="#">{{$post->user()->first()->name}}</a></div>
                                        <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                                    </div>
                                    <h3 class="heading mt-3"><a href="#">{{$post->titre}}</a></h3>
                                    <p>{{$post->contenu}}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="blog-entry align-self-stretch">
                            <a href="blog-single.html" class="block-20"
                               style="background-image: url({{asset('images/image_1.jpg')}});">
                            </a>
                            <div class="text p-4 d-block">
                                <div class="meta mb-3">
                                    <div><a href="#">Sept 10, 2018</a></div>
                                    <div><a href="#">Admin</a></div>
                                    <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                                </div>
                                <h3 class="heading mt-3"><a href="#">Hurricane Irma has devastated Florida</a></h3>
                                <p>A small river named Duden flows by their place and supplies it with the necessary
                                    regelialia.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="blog-entry align-self-stretch">
                            <a href="blog-single.html" class="block-20"
                               style="background-image: url({{asset('images/image_2.jpg')}});">
                            </a>
                            <div class="text p-4 d-block">
                                <div class="meta mb-3">
                                    <div><a href="#">Sept 10, 2018</a></div>
                                    <div><a href="#">Admin</a></div>
                                    <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                                </div>
                                <h3 class="heading mt-3"><a href="#">Hurricane Irma has devastated Florida</a></h3>
                                <p>A small river named Duden flows by their place and supplies it with the necessary
                                    regelialia.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="blog-entry align-self-stretch">
                            <a href="blog-single.html" class="block-20"
                               style="background-image: url({{asset('images/image_3.jpg')}});">
                            </a>
                            <div class="text p-4 d-block">
                                <div class="meta mb-3">
                                    <div><a href="#">Sept 10, 2018</a></div>
                                    <div><a href="#">Admin</a></div>
                                    <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                                </div>
                                <h3 class="heading mt-3"><a href="#">Hurricane Irma has devastated Florida</a></h3>
                                <p>A small river named Duden flows by their place and supplies it with the necessary
                                    regelialia.</p>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>

    <section class="ftco-section bg-light">
        <div class="container">
            <div class="row justify-content-center mb-5 pb-3">
                <div class="col-md-7 heading-section ftco-animate text-center">
                    <h2 class="mb-4">Nos derniers événements</h2>
                </div>
            </div>
            <div class="row">
                @if(isset($events) && !empty($events))
                    @foreach($events as $event)
                        <div class="col-md-4 d-flex ftco-animate">
                            <div class="blog-entry align-self-stretch">
                                <a href="blog-single.html" class="block-20"
                                   style="background-image: url({{ asset('storage/'.$event->image) }});">
                                </a>
                                <div class="text p-4 d-block">
                                    <div class="meta mb-3">
                                        <div><a href="#">{{$event->date}}</a></div>
                                        <div><a href="#">Admin</a></div>
                                        <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                                    </div>
                                    <h3 class="heading mb-4"><a href="#">{{$event->titre}}</a></h3>
                                    <p class="time-loc"><span class="mr-2"><i
                                                    class="icon-clock-o"></i> {{$event->heure_debut}}</span> <span><i
                                                    class="icon-map-o"></i> {{$event->lieu}}</span></p>
                                    <p class="preview-content">{{$event->description}}</p>
                                    <p><a href="event.html">Voir plus <i class="ion-ios-arrow-forward"></i></a></p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="blog-entry align-self-stretch">
                            <a href="blog-single.html" class="block-20"
                               style="background-image: url({{asset('images/event-1.jpg')}});">
                            </a>
                            <div class="text p-4 d-block">
                                <div class="meta mb-3">
                                    <div><a href="#">Sep. 10, 2018</a></div>
                                    <div><a href="#">Admin</a></div>
                                    <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                                </div>
                                <h3 class="heading mb-4"><a href="#">World Wide Donation</a></h3>
                                <p class="time-loc"><span class="mr-2"><i
                                                class="icon-clock-o"></i> 10:30AM-03:30PM</span>
                                    <span><i class="icon-map-o"></i> Venue Main Campus</span></p>
                                <p>A small river named Duden flows by their place and supplies it with the necessary
                                    regelialia.</p>
                                <p><a href="event.html">Join Event <i class="ion-ios-arrow-forward"></i></a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="blog-entry align-self-stretch">
                            <a href="blog-single.html" class="block-20"
                               style="background-image: url({{asset('images/event-2.jpg')}});">
                            </a>
                            <div class="text p-4 d-block">
                                <div class="meta mb-3">
                                    <div><a href="#">Sep. 10, 2018</a></div>
                                    <div><a href="#">Admin</a></div>
                                    <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                                </div>
                                <h3 class="heading mb-4"><a href="#">World Wide Donation</a></h3>
                                <p class="time-loc"><span class="mr-2"><i
                                                class="icon-clock-o"></i> 10:30AM-03:30PM</span>
                                    <span><i class="icon-map-o"></i> Venue Main Campus</span></p>
                                <p>A small river named Duden flows by their place and supplies it with the necessary
                                    regelialia.</p>
                                <p><a href="event.html">Join Event <i class="ion-ios-arrow-forward"></i></a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="blog-entry align-self-stretch">
                            <a href="blog-single.html" class="block-20"
                               style="background-image: url({{asset('images/event-3.jpg')}});">
                            </a>
                            <div class="text p-4 d-block">
                                <div class="meta mb-3">
                                    <div><a href="#">Sep. 10, 2018</a></div>
                                    <div><a href="#">Admin</a></div>
                                    <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
                                </div>
                                <h3 class="heading mb-4"><a href="#">World Wide Donation</a></h3>
                                <p class="time-loc"><span class="mr-2"><i
                                                class="icon-clock-o"></i> 10:30AM-03:30PM</span>
                                    <span><i class="icon-map-o"></i> Venue Main Campus</span></p>
                                <p>A small river named Duden flows by their place and supplies it with the necessary
                                    regelialia.</p>
                                <p><a href="event.html">Join Event <i class="ion-ios-arrow-forward"></i></a></p>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>

    <section class="ftco-section-3 img" id="contact" style="background-image: url({{asset('images/bg_4.jpg')}});">
        <div class="overlay"></div>
        <div class="container">
            <div class="row d-md-flex">
                <div class="col-md-6 d-flex ftco-animate">
                    <div class="img img-2 align-self-stretch" style="background-image: url({{asset('images/bg_4.jpg')}});"></div>
                </div>
                <div class="col-md-6 volunteer pl-md-5 ftco-animate">
                    <h3 class="mb-3">Devenez volontaire</h3>
                    <form action="{!! route('contact') !!}" method="post" class="volunter-form">
                        @csrf
                        <div class="form-group">
                            <input type="hidden" name="subject" id="subject" value="Devenir volontaire" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="text" name="name" id="name" class="form-control" placeholder="Votre nom" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="email" id="email" class="form-control" placeholder="Votre Email" required>
                        </div>
                        <div class="form-group">
                            <textarea name="message" id="message" cols="30" rows="3" class="form-control"
                                      placeholder="Message" required></textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Envoyer" class="btn btn-white py-3 px-5">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection