@extends("layouts.app-site")
@section('slide-image')
    @if(isset($page->slide)) {{asset('storage/'.$page->slide)}} @else 'images/bg_2.jpg' @endif
@endsection
@section('breadcrumb').
@parent
@section('bread-page') Gallery  @endsection
@endsection
@section('title')
    @if(isset($page->slide_title)) {{$page->slide_title}} @else Galleries @endif
@endsection
@section('content')
    <section class="ftco-section ftco-gallery">
        <div class="container">
            @if(isset($galleries) && !empty($galleries))
                @foreach($galleries as $k=>$gallery)
                    @if($k % 4 == 0)
                        <div class="d-md-flex">
                            @endif
                            <a href="{{ asset('storage/'.$gallery->image) }}"
                               class="gallery image-popup d-flex justify-content-center align-items-center img ftco-animate"
                               style="background-image: url({{ asset('storage/'.$gallery->image) }});">
                                <div class="icon d-flex justify-content-center align-items-center">
                                    <span class="icon-search"></span>
                                </div>
                            </a>
                            @if($loop->last || ($k+1) % 4 == 0)
                        </div>
                    @endif
                @endforeach
            @else
                <div class="d-md-flex">
                    <a href="images/cause-2.jpg"
                       class="gallery image-popup d-flex justify-content-center align-items-center img ftco-animate"
                       style="background-image: url(images/cause-2.jpg);">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <span class="icon-search"></span>
                        </div>
                    </a>
                    <a href="images/cause-3.jpg"
                       class="gallery image-popup d-flex justify-content-center align-items-center img ftco-animate"
                       style="background-image: url(images/cause-3.jpg);">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <span class="icon-search"></span>
                        </div>
                    </a>
                    <a href="images/cause-4.jpg"
                       class="gallery image-popup d-flex justify-content-center align-items-center img ftco-animate"
                       style="background-image: url(images/cause-4.jpg);">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <span class="icon-search"></span>
                        </div>
                    </a>
                    <a href="images/cause-5.jpg"
                       class="gallery image-popup d-flex justify-content-center align-items-center img ftco-animate"
                       style="background-image: url(images/cause-5.jpg);">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <span class="icon-search"></span>
                        </div>
                    </a>
                </div>
                <div class="d-md-flex">
                    <a href="images/cause-6.jpg"
                       class="gallery image-popup d-flex justify-content-center align-items-center img ftco-animate"
                       style="background-image: url(images/cause-6.jpg);">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <span class="icon-search"></span>
                        </div>
                    </a>
                    <a href="images/image_3.jpg"
                       class="gallery image-popup d-flex justify-content-center align-items-center img ftco-animate"
                       style="background-image: url(images/image_3.jpg);">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <span class="icon-search"></span>
                        </div>
                    </a>
                    <a href="images/image_1.jpg"
                       class="gallery image-popup d-flex justify-content-center align-items-center img ftco-animate"
                       style="background-image: url(images/image_1.jpg);">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <span class="icon-search"></span>
                        </div>
                    </a>
                    <a href="images/image_2.jpg"
                       class="gallery image-popup d-flex justify-content-center align-items-center img ftco-animate"
                       style="background-image: url(images/image_2.jpg);">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <span class="icon-search"></span>
                        </div>
                    </a>
                </div>
                <div class="d-md-flex">
                    <a href="images/event-1.jpg"
                       class="gallery image-popup d-flex justify-content-center align-items-center img ftco-animate"
                       style="background-image: url(images/event-1.jpg);">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <span class="icon-search"></span>
                        </div>
                    </a>
                    <a href="images/event-2.jpg"
                       class="gallery image-popup d-flex justify-content-center align-items-center img ftco-animate"
                       style="background-image: url(images/event-2.jpg);">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <span class="icon-search"></span>
                        </div>
                    </a>
                    <a href="images/image_1.jpg"
                       class="gallery image-popup d-flex justify-content-center align-items-center img ftco-animate"
                       style="background-image: url(images/image_4.jpg);">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <span class="icon-search"></span>
                        </div>
                    </a>
                    <a href="images/image_2.jpg"
                       class="gallery image-popup d-flex justify-content-center align-items-center img ftco-animate"
                       style="background-image: url(images/event-4.jpg);">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <span class="icon-search"></span>
                        </div>
                    </a>
                </div>
            @endif
        </div>
    </section>

    <section class="ftco-section-3 img" style="background-image: url({{asset('images/bg_4.jpg')}});">
        <div class="overlay"></div>
        <div class="container">
            <div class="row d-md-flex">
                <div class="col-md-6 d-flex ftco-animate">
                    <div class="img img-2 align-self-stretch" style="background-image: url({{asset('images/bg_4.jpg')}});"></div>
                </div>
                <div class="col-md-6 volunteer pl-md-5 ftco-animate">
                    <h3 class="mb-3">Devenez volontaire</h3>
                    <form action="{!! route('contact') !!}" method="post" class="volunter-form">
                        @csrf
                        <div class="form-group">
                            <input type="hidden" name="subject" id="subject" value="Devenir volontaire" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="text" name="name" id="name" class="form-control" placeholder="Votre nom" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="email" id="email" class="form-control" placeholder="Votre Email" required>
                        </div>
                        <div class="form-group">
                            <textarea name="message" id="message" cols="30" rows="3" class="form-control"
                                      placeholder="Message" required></textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Envoyer" class="btn btn-white py-3 px-5">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection